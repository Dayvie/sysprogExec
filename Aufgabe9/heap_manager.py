"""
Class which represents a Heap of an OS
"""

import collections
import math


class Memory_Sequence:

    def __init__(self, flag, size, allocated_memory):
        self.flag = flag
        self.size = size
        self.allocated_memory = allocated_memory


class Heap(object):

    def __init__(self):
        self.heap_max_size = 32768
        self.heap = []

    def allocate(self, size):
        for memory_location in self.heap:
            if memory_location.size >= size:
                if memory_location.flag == False:
                    memory_location.flag = True
                    return memory_location
        memory_tuple = self.sys_break(size)
        memory_tuple.flag = True
        return memory_tuple

    def sys_break(self, size):
        pages = math.floor(size / 4096)
        memory_tuple = Memory_Sequence(False, ((pages + 1) * 4096), (pages + 1) * 4096)
        self.heap.append(memory_tuple)
        return memory_tuple

    def deallocate(self, memory_tuple):
        for memory_location in self.heap:
            if memory_location == memory_tuple:
                memory_location.flag = False
                print("Memory Deallocated")

def show_heap_content(heap):
    for memory_tuple in heap:
        print(str(memory_tuple.flag) + " " + str(memory_tuple.size) + " " + str(memory_tuple.allocated_memory))

if __name__ == "__main__":
    heap = Heap()
    memory_tuple = heap.allocate(6012)
    memory_tuple2 = heap.allocate(1337)
    show_heap_content(heap.heap)
    heap.deallocate(memory_tuple)
    show_heap_content(heap.heap)
    memory_tuple3 = heap.allocate(2)
    show_heap_content(heap.heap)

