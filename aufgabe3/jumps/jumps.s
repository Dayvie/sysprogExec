# Finding a Maximum, Bartlett PGU Chapter 3

    # ----- data section -----
    .section .data

data_items:

    # ----- text section -----
    .section .text
    .globl _start #_kek bei entfernen: symbol name erwartet Fehler 1 bei anderem(_kek): cannot find entry symbol

_start: #_kek:
    movl    $-1, %eax
    cmpl    $5, %eax
    je	    _exit1
    jle     _exit2
    
_exit1:
    movl    $5, %ebx
    movl    $1, %eax
    int     $0x80
    
_exit2:
    movl    $-1, %ebx
    movl    $1,  %eax
    int     $0x80
    
_exit3:
    movl    $6, %ebx
    movl    $1, %eax
    int     $0x80


# vim: expandtab ts=4 sw=4
