.section .data

  array:
    .long   3, 67, 34, 200, 17, array_end

array_end:

.section .text

  .global _start

  _start:
    movl array, %ebx #direct addressing mode
    movl $0, %ecx
    movl array(,%ecx,4), %ebx #indexed addressing mode
    movl $array, %ecx
    movl (%ecx), %ebx #indirect addressing mode
    movl 8(%ecx), %ebx #base pointer addressing mode


  _end:
    movl $1, %eax
    int $0x80
