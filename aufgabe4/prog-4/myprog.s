.section .datah
data_items:
    .long   3, 220, 34, 200, 17, data_end
    
data_end:
    
.section .text
    .globl _start
    .globl quadrat
    .globl maximum_start
    
    _start:
#    pushl $ 5          # argument for the pow function
    call maximum_start  # run the function
#    addl $4, %esp      # Scrubs the parameter that was pushed on
                       # the stack, not needed when no parameter was used
    movl %eax, %ebx    # factorial returns the answer in %eax, but
                       # we want it in %ebx to send it as our exit
                       # status

    movl $1, %eax      # call the kernel's exit function
    int $0x80
    
quadrat:
    pushl %ebp         # standard function stuff - we have to
                       # restore %ebp to its prior state before
                       # returning, so we have to push it
    movl %esp, %ebp    # This is because we don't want to modify
		       # the stack pointer, so we use %ebp.
    movl 8(%ebp), %eax
    imull %eax, %eax
    
end_quadrat:
    movl %ebp, %esp    # standard function return stuff - we
    popl %ebp          # have to restore %ebp and %esp to where
                       # they were before the function started
    ret
    
maximum_start:
    pushl %ebp
    movl %esp, %ebp 
    movl    $0, %edi # set the array index to 0
    movl    data_items(, %edi, 4), %ebx # move the first item of the array to ebx
    jmp maximum
  
maximum:
    incl    %edi
    cmpl    $data_end, data_items(, %edi, 4)
    je      end_maximum
    movl    data_items(, %edi, 4), %eax
    cmpl    %ebx, %eax
    jle     maximum
    movl    %eax, %ebx
    jmp     maximum
    
end_maximum:
    movl %ebx, %eax # put the value of ebx in eax, %eax holds the return value
    movl %ebp, %esp # point the frame pointer on the stack pointer    
    popl %ebp       # pop the base pointer  
    ret
    