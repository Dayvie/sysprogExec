import sys

def to_upper_case(source, destination):
    """
    :param source: name of the sourcefile
    :param destination: name of the destinationfile
    :return: void
    """
    with open(source, "r") as source_process, open(destination, 'w') as dest:
        for line in source_process.readlines():
            s = str(line.strip())
            dest.write(s.upper() + "\n")
            print(s)
    print("finished writing")

if __name__ == "__main__":
    print(sys.argv)
    arg1 = sys.argv[1]
    arg2 = sys.argv[2]
    print("first argument: " + arg1)
    print("second argument: " + arg2)
    to_upper_case(arg1, arg2)