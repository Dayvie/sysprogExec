# a simple programm to write a string into a file
# the filename is stored in $file_name
# the output is stored in $ file_content

        .section .data
        # system call numbers
        .equ SYS_OPEN, 5
        .equ SYS_WRITE, 4
        .equ SYS_READ, 3
        .equ SYS_CLOSE, 6
        .equ SYS_EXIT, 1
        .equ O_CREAT_WRONLY_TRUNC, 03101 # write intention
        .equ UNIX_PERMISSIONS, 0666
        .equ SYS_CALL, 0x80 # system call interrupt

        # values
file_name:
        .asciz "heyDiddle.txt"
file_content:
        .asciz "Hey diddle diddle"
content_size:
        .int content_size - file_content

        .section .bss
        .section .text
        .globl _start
_start:
open_file:
        movl $SYS_OPEN, %eax # open the file
        movl $file_name, %ebx # put file_name adress in %ebx
        movl $O_CREAT_WRONLY_TRUNC, %ecx # read / write intentions
        movl $UNIX_PERMISSIONS, %edx # unix permissions
        int $SYS_CALL # work

store_file:
        movl %eax, %ebp # store the fd in %ebp

write_file:
        movl $SYS_WRITE, %eax # write into the file
        movl %ebp, %ebx # use this file
        movl $file_content, %ecx # location of buffer to write into the file
        # the size of the buffer
        movl content_size, %edx # work
        int $SYS_CALL

close_file: # close a opened file
        movl $SYS_CLOSE, %eax # close this file
        movl %ebp, %ebx 
        int $SYS_CALL

_exit: # exit the programm
        movl $SYS_EXIT, %eax # return 0
        movl $0, %ebx
        int $SYS_CALL
