(gdb) x/1xw  $esp+8     # Anzahl Parameter

(gdb) x/s  *(int*)($esp+12)   # Parameter 1
   0xbff6f78b:      "/home/hhoegl/svn/Vorlesungen/sysprog/code/prog-5/toupper"

(gdb) x/s  *(int*)($esp+16)   # Parameter 2
0xbff6f7c4:      "infile.txt"

(gdb) x/s  *(int*)($esp+20)   # Parameter 3
0xbff6f7cf:      "outfile.txt"

