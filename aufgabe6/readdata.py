import struct

FMT = "40s40s240si"

n = struct.calcsize(FMT)

fo = open("test.dat", "r")
s = fo.read()
m = len(s)

i = 0
while i < m:
    T = struct.unpack(FMT, s[i:i+n])
    print T
    print "---"
    i += n

fo.close()
