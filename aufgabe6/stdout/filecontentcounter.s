# -*- indent-tabs-mode: nil -*- (for Emacs)

# PURPOSE: This program converts an input file
# to an output file with all letters
# converted to uppercase.
#
# PROCESSING:
# 1) Open the input file
# 2) Open the output file
# 4) While we're not at the end of the input file
#    a) read part of file into our memory buffer
#    b) go through each byte of memory
#       if the byte is a lower-case letter,
#       convert it to uppercase
#    c) write the memory buffer to output file


        .section .data

        #######CONSTANTS########
        # system call numbers
        .equ SYS_OPEN, 5
        .equ SYS_WRITE, 4
        .equ SYS_READ, 3
        .equ SYS_CLOSE, 6
        .equ SYS_EXIT, 1

        # options for open (look at
        # /usr/include/asm/fcntl.h for
        # various values. You can combine them
        # by adding them or ORing them)
        # This is discussed at greater length
        # in "Counting Like a Computer"
        .equ O_RDONLY, 0
        .equ O_CREAT_WRONLY_TRUNC, 03101

        # standard file descriptors
        .equ STDIN, 0
        .equ STDOUT, 1
        .equ STDERR, 2

        # system call interrupt
        .equ LINUX_SYSCALL, 0x80
        .equ END_OF_FILE, 0
        .equ NUMBER_ARGUMENTS, 1

        .section .bss

        # Buffer - this is where the data is loaded into
        # from the data file and written from
        # into the output file. This should
        # never exceed 16,000 for various
        # reasons.
        .equ BUFFER_SIZE, 500
        .lcomm BUFFER_DATA, BUFFER_SIZE

        .section .text

        # STACK POSITIONS
        .equ ST_SIZE_RESERVE, 4
        .equ ST_FD_IN, -4
        .equ counter_l, -8
        .equ ST_ARGC, 0         # Number of arguments
        .equ ST_ARGV_0, 4       # Name of program
        .equ ST_ARGV_1, 8       # Input file name
        .globl _start

_start:
        ### INITIALIZE PROGRAM ###
        movl %esp, %ebp
        subl $ST_SIZE_RESERVE, %esp

open_fd_in:
        movl $SYS_OPEN, %eax
        movl ST_ARGV_1(%ebp), %ebx
        movl $O_RDONLY, %ecx
        movl $0666, %edx
        int $LINUX_SYSCALL
        movl %eax, ST_FD_IN(%ebp)

read_loop_begin:
                 # READ IN A BLOCK FROM THE INPUT FILE
        movl $SYS_READ, %eax
        movl ST_FD_IN(%ebp), %ebx  # get the input file descriptor
        movl $BUFFER_DATA, %ecx    # the location to read into
        movl $BUFFER_SIZE, %edx    # the size of the buffer
        int $LINUX_SYSCALL         # Size of buffer read is returned in %eax
        cmpl $END_OF_FILE, %eax    # check for end of file marker
        jle  printresult           # if found or on error, go to the end
        jmp read_loop_begin

printresult:
        movl $4, %eax
        movl $STDOUT, %ebx
        movl $BUFFER_DATA, %ecx
        movl $BUFFER_SIZE, %edx
        int $0x80

endprogramm:
        movl $1, %eax
        movl $0, %ebx
        int $0x80
