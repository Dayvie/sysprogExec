.section .data

.equ STDOUT, 1

file_content:
  .asciz "Hello World!\n"

content_size:
  .int content_size - file_content

.section .bss
.section .text
.globl _start

_start:

_printHelloWorld:
      movl $4, %eax
      movl $STDOUT, %ebx
      movl $file_content, %ecx
      movl content_size, %edx
      int $0x80

_getthefuckout:
      movl $1, %eax
      movl $0, %ebx
      int $0x80
