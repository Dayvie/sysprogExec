#######CONSTANTS#######
      # system call numbers #
      .equ SYS_OPEN, 5
      .equ SYS_WRITE, 4
      .equ SYS_READ, 3
      .equ SYS_CLOSE, 6
      .equ SYS_EXIT, 1

      .equ LINUX_SYSCALL, 0x80

      .equ O_RDONLY, 0

      .equ PROG_ARG_1, 8
      .equ BUFFER_SIZE, 512

      .equ RF_ARGS_FD, 8
      .equ RF_BYTES_READ, -4
      .equ RF_NEW_LINE, 10

      .equ MAIN_C_COUNT, -4
      .equ MAIN_L_COUNT, -8

      .section .data
          LINE_COUNT:
              .asciz "Number of lines: %d"
          CHAR_COUNT:
              .asciz "Number of chars: %d"

      .section .bss
      .lcomm DATA_BUFFER, BUFFER_SIZE
      .lcomm RF_RETURN, 8

      .section .text
      .globl _start

  _start:
      movl %esp, %ebp
      subl $8, %esp         # reserve some space for local variables on the stack

  open_file:
      movl $SYS_OPEN, %eax
      movl PROG_ARG_1(%ebp), %ebx
      movl $O_RDONLY, %ecx
      movl $0666, %edx
      int $LINUX_SYSCALL

      pushl %eax
      call read_file
      movl (%eax), %ebx
      movl %ebx, MAIN_C_COUNT(%ebp)
      movl 4(%eax), %ebx
      movl %ebx, MAIN_L_COUNT(%ebp)
      addl $4, %esp

      popl %ebx
      addl $4, %esp

  close_files:
      movl $SYS_CLOSE, %eax # the filedescriptor is already in ebx
      int $LINUX_SYSCALL

  exit:
      movl $SYS_EXIT, %eax
      movl MAIN_L_COUNT(%ebp), %ebx
      int $LINUX_SYSCALL


      # Reads a file and couts the caracters and lines
      # INPUT:
      #       Filedescriptor
      # OUTPUT:
      #       Number of lines
      # REGISTERS:
      #       eax: number of bytes read from the file
      #       ebx: number of characters
      #       ecx: number of lines

      .type read_file, @function
  read_file:
      pushl %ebp
      movl %esp, %ebp

      subl $8, %esp
      pushl $0
      pushl $0

  read_file_start:
      movl $SYS_READ, %eax
      movl RF_ARGS_FD(%ebp), %ebx
      movl $DATA_BUFFER, %ecx
      movl $BUFFER_SIZE, %edx
      int $LINUX_SYSCALL

      popl %ebx                           # restore the counters
      popl %ecx

      cmpl $0, %eax                       # exit the function if there is no more data to read
      je read_file_exit

  read_file_process_character:
      decl %eax
      cmpb $RF_NEW_LINE, DATA_BUFFER(%eax)
      jne read_file_count_char
      incl %ecx

  read_file_count_char:
      incl %ebx

      cmpl $0, %eax
      jne read_file_process_character

      pushl %ecx                          # save the counters
      pushl %ebx

      jmp read_file_start                 # read the next junk of data

  read_file_exit:

      movl $RF_RETURN, %eax
      movl %ebx, (%eax)
      movl %ecx, 4(%eax)

      movl %ebp, %esp
      popl %ebp
      ret
