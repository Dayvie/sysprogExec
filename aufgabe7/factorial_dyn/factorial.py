

def factorial(n):
    if n == 1:
        return n
    else:
        return factorial(n-1) * n

for n in range(1, 21):
    print n, factorial(n)
