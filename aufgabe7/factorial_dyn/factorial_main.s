# Bartlett, S. 65
# @author David Yesil
# PURPOSE - Given a number, this program computes the
# factorial. For example, the factorial of
# 3 is 3 * 2 * 1, or 6. The factorial of
# 4 is 4 * 3 * 2 * 1, or 24, and so on.
#
# This program shows how to call a function recursively.

    .section .data

my_string:
  .ascii "%d\n\0"

    .section .text
    .globl _start

    _start:
    pushl $4           # The factorial takes one argument - the
                       # number we want a factorial of. So, it
                       # gets pushed
    call factorial     # run the factorial function
    addl $4, %esp      # Scrubs the parameter that was pushed on
    pushl %eax
    pushl $my_string
    call printf
    addl $4, %esp
    pushl $0
    call exit
