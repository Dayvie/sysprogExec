# ----- data section ----- @author David Yesil
    .section .data

my_string:
  .ascii "Die groesste Zahl in der Liste ist: %d\n"

data_items:
    .long   3, 67, 34, 200, 17, data_end
   #.long   3, 67, 34, 1022, 17, 0
   #.long   3, 67, 34, 1022, 17, 255 um mit 255 zu verlassen

data_end:

    # ----- text section -----
    .section .text
    .globl _start

_start:
    movl    $0, %edi
    movl    data_items(, %edi, 4), %eax
    movl    %eax, %ebx

start_loop:
   #cmpl    $255, %eax um mit 255 zu verlassen
    incl    %edi
    cmpl    $data_end, data_items(, %edi, 4)
    je      loop_exit
    movl    data_items(, %edi, 4), %eax
    cmpl    %ebx, %eax
    jle     start_loop
    movl    %eax, %ebx
    jmp     start_loop

loop_exit:
#   movl    $3, %ebx um 3 zurückzugeben
    pushl %ebx
    pushl $my_string
    call printf
    addl $4, %esp
    pushl %ebx
    call exit

#movl $_start, %eax läd die Addresse des symbols _start in %eax
#movl _start, %eax schreibt 4 bytes von der speicheraddresse in %eax
