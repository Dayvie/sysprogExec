#include <stdio.h>

extern int getcycles( int (*fkt)() );
extern int dw_cycles;

int f()
{
    int i;
    for (i=0; i<2000; i++) {
        /* empty */
    }
    return 1;
}

int main()
{
    int n;
    n = getcycles(f);               
    printf("0x%x\n", n);
    return 0;
}

/* 
   vim: et sw=4 ts=4
*/
