#include <stdio.h>
#include <string.h>

int main(int argc, char *argv[], char *env[])
{
    int i;

    printf("argc = %d\n", argc);

    i = 0;
    while (argv[i]) {
        printf("argv[%d] = %s\n", i, argv[i]);
        i++;  /* identisch mit i = i + 1; */
    }

    i = 0;
    while (env[i]) {
        printf("env[%d] = %s\n", i, env[i]);
        i++; 
    }
    return 0;
}

/* vim: set et ts=4 sw=4: */
