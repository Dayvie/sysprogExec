#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>


int main()
{
    int fd, n;
    char *buffer[80];

    fd = open("daten", O_RDONLY);
    do {
        n = read(fd, buffer, 4);
        printf("n = %d\n", n);
    } while (n > 0);
    close(fd);
    return 0;
}

/* vim: set et ts=4 sw=4: */
