#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>


int main()
{
    int fd;
    char *cp = "Donaudampfschiffkapitaen";

    fd = open("daten", O_RDWR|O_CREAT, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP);
    write(fd, cp, strlen(cp));
    close(fd);
    return 0;
}

/* vim: set et ts=4 sw=4: */
