#include <stdio.h>
#include <stdlib.h>

#define BUFLEN 128

int main()
{
    int n;
    char buffer[BUFLEN];

    /* ascii to integer */
    n = atoi("2008");
    printf("n = %d\n", n);

    /* integer to ascii */
    snprintf(buffer, BUFLEN, "%d\n", 2008);
    printf("n = %s\n", buffer);

    return 0;
}


/* vim: set et ts=4 sw=4: */

