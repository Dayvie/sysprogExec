#include <stdio.h>


int Liste[] = {3, 67, 34, 128, 17, 0};

int main()
{
    int i = 0, max = 0;
    do {
        if (Liste[i] > max) {
            max = Liste[i];
        }
        i = i + 1;
    } while (Liste[i] != 0);
    printf("Das Maximum ist %d.\n", max);    
    return 0;
}

/* vim: set et ts=4 sw=4: */

