#include <stdio.h>

int factorial(int n)
{
    if (n == 1) {
        return n;
    }
    else {
        return factorial(n-1) * n;
    }
}

int main()
{
    int n = 5;
    printf("Factorial of %d is %d\n", n, factorial(n));
    return 0;
}


/* vim: set et ts=4 sw=4: */

