#include <stdio.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <ctype.h>   /* toupper */

#define BUFLEN 500

char buffer[BUFLEN];


void convert_to_upper();


int main()
{
    int n, m, fd_in, fd_out;

    fd_in = open("infile.txt", O_RDONLY);
    fd_out = open("outfile.txt", O_RDWR|O_CREAT, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP);

    do {
        n = read(fd_in, buffer, BUFLEN);
        convert_to_upper(n, buffer);
        write(fd_out, buffer, n);
        /*
        if (n > 0) {
            for (m=0; m<n; m++) {
                buffer[m] = (char)toupper( (int)buffer[m] );
            }
            write(fd_out, buffer, n);
        }
        */
        printf("n = %d\n", n);
    } while (n > 0);

    close(fd_in);
    close(fd_out);
}


void convert_to_upper(int len, char *buffer)
{
   char *cp = buffer;
   if (len == 0) {
      return;
   }
   while (1) {
      if (*cp == 0) {
          break;
      }
      if ( (*cp < 'a') || (*cp > 'z')) {
           /* ignore (not a character) */
      }
      else {
          *cp = *cp - ('a' - 'A');
      }
      cp++;
   }
   return;
}


/* vim: set et ts=4 sw=4: */

