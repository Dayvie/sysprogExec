#include <stdio.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <ctype.h>   /* toupper */

#define BUFLEN 500

char buffer[BUFLEN];


int main()
{
    int n, m, fd_in, fd_out;

    fd_in = open("infile.txt", O_RDONLY);
    fd_out = open("outfile.txt", O_RDWR|O_CREAT, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP);

    do {
        n = read(fd_in, buffer, BUFLEN);
        if (n > 0) {
            for (m=0; m<n; m++) {
                buffer[m] = (char)toupper( (int)buffer[m] );
            }
            write(fd_out, buffer, n);
        }
        printf("n = %d\n", n);
    } while (n > 0);

    close(fd_in);
    close(fd_out);
}



/* vim: set et ts=4 sw=4: */

