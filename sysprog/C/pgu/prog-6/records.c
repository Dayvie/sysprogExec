#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>


struct record {
    char firstname[40];
    char lastname[40];
    char address[240];
    int age;
};


int read_record(int fd, struct record *buffer);
int write_record(int fd, char *buffer);


struct record r1 = {
    .firstname = "Frederick",
    .lastname = "Bartlett",
    .address = "4242 S Prairie\nTulsa, OK 55555",
    .age = 45,
};

struct record r2 = {
    .firstname = "Marilyn",
    .lastname = "Taylor",
    .address = "2224 S Johannan St\nChicago, IL 12345",
    .age = 29,
};

struct record r3 = {
    .firstname = "Derrick",
    .lastname = "McIntire",
    .address = "500 W Oakland\nSan Diego, CA 54321",
    .age = 36,
};


int read_record(int fd, struct record *buffer)
{
    return read(fd, (char *)buffer, sizeof(struct record));
}


int write_record(int fd, char *buffer)
{
    return write(fd, buffer, sizeof(struct record));
}


int write_records()
{
    int fd;
    fd = open("test.dat", O_RDWR|O_CREAT, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP);
    write_record(fd, (char*)&r1);
    write_record(fd, (char*)&r2);
    write_record(fd, (char*)&r3);
    close(fd);
    return 0;
}


int read_records()
{
   int n, fd;
   struct record buffer[1];

   fd = open("test.dat", O_RDONLY);
   while (1) {
      n = read_record(fd, buffer);
      if (n == 0) {
          break;
      }
      printf("%s\n", buffer->firstname);
   }
   close(fd);
   return 0;
}


int main()
{
    write_records();
    read_records();
    return 0;
}
    

/* vim: set et ts=4 sw=4: */
