/* Bartlett, chapter 9, intermediate memory topics */

/*
 * ENTWURF - NOCH NICHT GETESTET!
 */

#include <stdio.h>
#include <unistd.h>

int mem[1000000];

#define HDRSZ 8
#define UNAVAIL 0
#define AVAIL 1


int *current_break = mem;
int *heap_begin = mem;

/* XXX to do: check if we are at the end of mem[] array */
void _set_hdr(int *p, int avail, int size)
{
   *p = avail;
   *(p+4) = size;
}


void _mysbrk(int *p, int size)
{
   current_break += (HDRSZ + size);
   *p = AVAIL;
   *(p+4) = size;
}


void allocate_init()
{

}

unsigned char *allocate(int size)
{
   int *p = heap_begin;

   if (p == current_break) {
      _mysbrk(heap_begin, size);       
   }
   while (1) {
      if (    (*p == AVAIL)
           && (*(p+4) >= size ) ) {
         return (unsigned char*)p+HDRSZ;
      }
      else if (   (*p == UNAVAIL)  
               && (p < current_break) ) {
         p = p + *(p+4);
         continue;
      }
      if (p == current_break) {
         _mysbrk(p, size); 
         return (unsigned char*)p+HDRSZ;
      }
   }

   return (unsigned char*) p+HDRSZ;
}


void deallocate(unsigned char *data)
{
    int *p = (int*) (data - HDRSZ);
    *(p+4) = AVAIL;
}


void break_test()
{
   void *b;

   b = sbrk((intptr_t) 0); 
   printf("current break is 0x%x\n", (unsigned int)b);
   
   b = sbrk(0x4000);
   printf("old break is 0x%x\n", (unsigned int)b);

   b = sbrk((intptr_t) 0); 
   printf("current break is 0x%x\n", (unsigned int)b);
}

int main()
{
   break_test();
   return 0;
}


/* 
vim: et sw=3 ts=3 
*/

