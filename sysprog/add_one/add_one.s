
# Bartlett Chapter 4: Functions

    # ----- data section -----
    .section .data
    # empty

    # ----- text section -----
    .section .text
    .globl _start

_start:
    pushl $4
    call add_one
    addl  $4, %esp

    # exit
    movl    %eax, %ebx
    movl    $1, %eax
    int     $0x80


add_one:
    pushl   %ebp
    movl    %esp, %ebp
    subl    $4, %esp
    movl    8(%ebp), %eax
    addl    $1, %eax
    movl    %eax, -4(%ebp)
    movl    %ebp, %esp
    popl    %ebp
    ret



# vim: expandtab ts=4 sw=4
