// go run add_one.go
// go build add_one.go
//
// H. Hoegl, 2014-11-06 

package main
import "os"


func add_one(a int)(t int) {
   t = a + 1
   return
}


func main() {
   var r int
   r = add_one(4)
   os.Exit(r)
}
