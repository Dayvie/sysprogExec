# addone.nim
# H. Hoegl, 2014-11-06 

var 
   r = 0

proc add_one(x: int): int =
   var t = x
   return t + 1

r = add_one(4)

echo("return value: ", $r)

quit(r)  # defined in system module
