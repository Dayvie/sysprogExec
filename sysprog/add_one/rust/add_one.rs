

fn add_one(n: u32) -> u32 {
   return n + 1;
}

fn main() {
    let mut rv = 4;
    println!("Input value = {}", rv);
    rv = add_one(4);
    println!("Return value = {}", rv);
}

