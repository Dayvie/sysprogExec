sysprog/gdb/Anleitung.rst
H. Hoegl, 2014-11-10 

1. Programm starten mit Argument

   gdb -tui -x _gdbinit
   (gdb) run bwk-tutorial.txt

2. Kommandozeilenargumente ansehen

   (gdb) p/x *(int*)$ebp
   $8 = 0x2

   (gdb) x/s *(char**)($ebp+4)
   0xbffff913:      "/home/hhoegl/Dienste/git/sysprog/gdb/fileread"

   (gdb) x/s *(char**)($ebp+8)
   0xbffff941:      "bwk-tutorial.txt"

3. Den open() Befehl mit ein paar "ni" Eingaben ausfuehren.

4. Rueckgabewert der open() Funktion:
  
   (gdb) p/x $eax
   $7 = 0x7

5. Den ersten Block aus Datei lesen und Buffer ansehen:

   (gdb) p/x &BUFFER_DATA     # Adresse von BUFFER_DATA

   (gdb) x/s &BUFFER_DATA     # Inhalt von BUFFER_DATA

6. Ausgeben einer Variable in der .data section:

   (gdb) p/x di    # hex
   $1 = 0x1f4

   (gdb) p di      # dezimal
   $2 = 500

   (gdb) p/x &di   # Adresse
   $1 = 0x80490c4

7. Geben Sie die Variablen ``dal``, ``dab`` und ``ds`` aus mit 
   geeigneten GDB Kommandos.

   Hinweis: Man kann das ``x`` (dump) Kommando anpassen auf die Daten,
   die man sehen will: ``x/5xb``, ``x/5xw``, ``x/s``.


