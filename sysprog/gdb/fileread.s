# sysprog/gdb/fileread.s        

        .equ SYS_OPEN, 5
        .equ SYS_WRITE, 4
        .equ SYS_READ, 3
        .equ SYS_CLOSE, 6
        .equ SYS_EXIT, 1

        .equ O_RDONLY, 0
        .equ O_CREAT_WRONLY_TRUNC, 03101

        .equ STDIN, 0
        .equ STDOUT, 1
        .equ STDERR, 2

        .section .data

di:     .long      500
dal:    .long      1, 3, 7, 11, 0
dab:    .byte      1, 3, 7, 11, 0
ds:     .asciz     "Hallo"

        # system call interrupt
        .equ LINUX_SYSCALL, 0x80
        .equ END_OF_FILE, 0 

        .equ NUMBER_ARGUMENTS, 2

        .section .bss

        .equ BUFFER_SIZE, 500
        .lcomm BUFFER_DATA, BUFFER_SIZE

        .section .text

        # STACK POSITIONS
        .equ ST_SIZE_RESERVE, 8
        .equ ST_FD_IN, -4
        .equ ST_FD_OUT, -8
        .equ ST_ARGC, 0         # Number of arguments
        .equ ST_ARGV_0, 4       # Name of program
        .equ ST_ARGV_1, 8       # Input file name
        .equ ST_ARGV_2, 12      # Output file name
        .globl _start

_start:
        movl %esp, %ebp

        subl $ST_SIZE_RESERVE, %esp

open_files:
open_fd_in:
        movl $SYS_OPEN, %eax
        movl ST_ARGV_1(%ebp), %ebx
        movl $O_RDONLY, %ecx
        movl $0666, %edx
        int $LINUX_SYSCALL
store_fd_in:
        movl %eax, ST_FD_IN(%ebp)

read_loop_begin:
        movl $SYS_READ, %eax
        movl ST_FD_IN(%ebp), %ebx  # get the input file descriptor
        movl $BUFFER_DATA, %ecx    # the location to read into
        movl $BUFFER_SIZE, %edx    # the size of the buffer
        int $LINUX_SYSCALL         # Size of buffer read is returned in %eax
        cmpl $END_OF_FILE, %eax    # check for end of file marker
                                   # if found or on error, go to the end
        jle end_loop

continue_read_loop:
        # empty ...
        jmp read_loop_begin

end_loop:
        movl $SYS_CLOSE, %eax
        movl ST_FD_IN(%ebp), %ebx
        int $LINUX_SYSCALL

        movl $SYS_EXIT, %eax
        movl $0, %ebx
        int $LINUX_SYSCALL

        
# vim: expandtab sw=8 sts=8
