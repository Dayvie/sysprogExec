
Systemnahe Programmierung
=========================

H\. Högl, Hochschule Augsburg

Programme aus dem Buch von Bartlett: `<pgu>`_ (in GNU Assembler)

  * `<pgu/prog-3-1>`_  Erstes Programm: Exit-Code
  * `<pgu/prog-3-2>`_  Maximum-Suche in einem Array
  * `<pgu/prog-4>`_ power.s und factorial.s
  * `<pgu/prog-5>`_ toupper.s
  * `<pgu/prog-6>`_
  * `<pgu/prog-7>`_
  * `<pgu/prog-8>`_
  * `<pgu/prog-9>`_
  * `<pgu/prog-10>`_

Sonstige Beispiele

* `<add_one/>`_, eine einfache Aufgabe zu Funktionen und Stack-Frames. 
  Ergänzung zu PGU-Buch, Kap. 4.  Das Beispiel gibt es auch in C, Go, Nim
  und Rust.

* `<gdb>`_, eine GDB Demo mit `Anleitung <gdb/Anleitung.rst>`_.

* `<Exploit>`_, eine Aufgabe von der früheren MNP Vorlesung.

* Beispiele aus "The C Book" `<The_C_Book>`_

* Beispiele aus PGU-Buch in `<C>`_ (und andere)

* Beispielprogramme in der Sprache `Go <go/>`_.

* Beispielprogramme in der Sprache `Rust <rust/>`_.

* Kurze `Tests <tests/>`_



