
    # Open file for reading
    movl $SYS_OPEN, %eax
    movl $input_file_name, %ebx
    movl $0, %ecx
    movl $0666, %edx
    int $LINUX_SYSCALL
    movl %eax, INPUT_DESCRIPTOR(%ebp)

    # This will test and see if %eax is
    # negative. If it is not negative, it
    # will jump to continue_processing.
    # Otherwise it will handle the error
    # condition that the negative number
    # represents.
    cmpl $0, %eax
    jl continue_processing

    # Send the error
    .section .data
no_open_file_code:
    .ascii "0001: \0"
no_open_file_msg:
    .ascii "Can't Open Input File\0"

    .section .text
    pushl $no_open_file_msg
    pushl $no_open_file_code
    call error_exit

continue_processing:
    # Rest of program        


# vim: expandtab ts=4 sw=4
